class Triangle {
    constructor(x,y,z) {
      this.x = x;
      this.y = y;
      this.z = z;
    }

    isCoherent() {
        if ((a+b)<=c || (b+c)<=a || (a+c)<=b){
            console.log('ce nest pas un triangle');
        }
    }


    isIsocele() {
        if (this.isCoherent() && ((this.x==this.y) || (this.y==this.z) || (this.x==this.z))){
            return true;
        } 
        else return false;
        }
  
   

    isEquilateral() {
        if (a == b && b == c){
            console.log('le triangle est equilateral');
        }
    }

    isRectangle() {
        }
  
}

  //////////////////////////////////////////////
  
  describe('Triangle test', () => {
    let triangle;
  
    beforeEach(() => {
      triangle = null;
    });

    it('toutes les fcts | est coherent et equilateral | expected: True - True - False', () => {
      triangle = new Triangle (1,1,1);
      expect(triangle.isCoherent()).toBe(true);
      expect(triangle.isEquilateral()).toBe(true);
      expect(triangle.isIsocele()).toBe(false);
    });

    it('toutes les fcts | (0,0,0) | expected: x3 False', () => {
      triangle = new Triangle (0,0,0);
      expect(triangle.isCoherent()).toBe(false);
      expect(triangle.isEquilateral()).toBe(false);
      expect(triangle.isIsocele()).toBe(false);      
    });

    it('isCoherent | negative input | expected: False', () => {
      triangle = new Triangle (-1,-1,-3);
      expect(triangle.isCoherent()).toBe(false);
    });

    it('isCoherent | impossible triangle | expected: False', () => {
      triangle = new Triangle (4,8,3);
      expect(triangle.isCoherent()).toBe(false);
    });

    it('isCoherent | ("a","b","c") | expected: False', () => {
      triangle = new Triangle ("a","b","c");
      expect(triangle.isCoherent()).toBe(false);
    });

    it('isCoherent | est un triangle', () => {
      triangle = new Triangle (4,8,5);
      expect(triangle.isCoherent()).toBe(true);
    });

    it('isCoherent | est un triangle', () => {
      triangle = new Triangle (4,11,5);
      expect(triangle.isCoherent()).toBe(false);
    });


    it('isIsocele | est isosceles | expected: True', () => {
      triangle = new Triangle (5,5,4);
      expect(triangle.isIsocele()).toBe(true);
    });

    it('isEquilateral | est equilateral', () => {
      triangle = new Triangle (5,5,5);
      expect(triangle.isEquilateral()).toBe(true);
    });
    it('isEquilateral | est equilateral', () => {
      triangle = new Triangle (8,5,5);
      expect(triangle.isEquilateral()).toBe(false);
    });
    it('isRectangle | est rectangle', () => {
      triangle = new Triangle (3,4,5);
      expect(triangle.isRectangle()).toBe(true);
    });
    it('isRectangle | est rectangle', () => {
      triangle = new Triangle (8,4,5);
      expect(triangle.isRectangle()).toBe(false);
    });

  });
